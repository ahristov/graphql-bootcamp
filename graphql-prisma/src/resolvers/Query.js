const Query = {
  users(parent, args, { prisma }, info) {
    const opArgs = {}

    if (args.query) {
      opArgs.where = {
        OR: [{
          name_contains: args.query
        }, {
          email_contains: args.query
        }]
      }
    }

    return prisma.query.users(opArgs, info)

    // opArgs: represents the query arguments
    // info: contains the query document
    //
    // NOTE: There is no case insensitive contains search.
    // For PostgreSQL eventually citext module may work (https://www.postgresql.org/docs/12/citext.html#id-1.11.7.17.5).
    //
    // {
    //   users(
    //     query: "12"
    //   ) {
    //     id
    //     name
    //     email
    //   }
    // }

  },
  posts(parent, args, { prisma }, info) {
    const opArgs = {}

    if (args.query) {
      opArgs.where = {
        OR: [{
          title_contains: args.query
        }, {
          body_contains: args.query
        }]
      }
    }

    return prisma.query.posts(opArgs, info)
  },
  comments(parent, args, { prisma }, info) {
    const opArgs = {}

    if (args.query) {
      opArgs.where = {
          text_contains: args.query
      }
    }

    return prisma.query.comments(opArgs, info)
  }
}

export { Query as default }