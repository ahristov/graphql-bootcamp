const Subscription = {
  comment: {
    async subscribe(parent, { postId }, { prisma }, info) {
      const opArgs = {
        where: {
          node: {
            post: {
              AND: [{
                published: true
              }]
            }
          }
        }
      }
      if (postId) {
        opArgs.where.node.post.AND.push({ id: postId })
      }

      return prisma.subscription.comment(opArgs, info)
    }
  },
  post: {
    async subscribe(parent, args, { prisma }, info) {
      return prisma.subscription.post({
        where: {
          node: {
            published: true
          }
        }
      }, info)
    }
  }
}

export { Subscription as default }