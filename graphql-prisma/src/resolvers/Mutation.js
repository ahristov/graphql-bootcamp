import { v4 as uuidv4 } from 'uuid'

const Mutation = {
  async createUser(parent, { data }, { prisma }, info) {
    const emailTaken = await prisma.exists.User({ email: data.email })

    if (emailTaken) {
      throw new Error('Email taken.')
    }

    return prisma.mutation.createUser({ data }, info)
  },
  async deleteUser(parent, { id }, { prisma }, info) {
    const userExists = await prisma.exists.User({ id })

    if (!userExists) {
      throw new Error('User not found.')
    }

    return prisma.mutation.deleteUser({ where: { id } }, info)
  },
  async updateUser(parent, { id, data }, { prisma }, info) {
    return prisma.mutation.updateUser({
      where: { id }, data
    }, info)
  },
  async createPost(parent, { data }, { prisma }, info) {
    const { title, body, published, author } = data
    return prisma.mutation.createPost({
      data: {
        title,
        body,
        published,
        author: {
          connect: {
            id: author
          }
        }
      }
    }, info)
  },
  async deletePost(parent, { id }, { prisma }, info) {
    return prisma.mutation.deletePost({ where: { id } }, info)
  },
  async updatePost(parent, { id, data }, { prisma }, info) {
    return prisma.mutation.updatePost({
      where: { id }, data
    }, info)
  },
  async createComment(parent, { data }, { prisma }, info) {
    const { text, author, post } = data
    return prisma.mutation.createComment({
      data: {
        text,
        author: {
          connect: {
            id: author
          }
        },
        post: {
          connect: {
            id: post
          }
        }
      }
    }, info)
  },
  async deleteComment(parent, { id }, { prisma }, info) {
    return prisma.mutation.deleteComment({ where: { id } }, info)
  },
  async updateComment(parent, { id, data }, { prisma }, info) {
    return prisma.mutation.updateComment({
      where: { id }, data
    }, info)
  }
}

export { Mutation as default }