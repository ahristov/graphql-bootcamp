import { Prisma } from 'prisma-binding'

const prisma = new Prisma({
  typeDefs: 'src/generated/prisma.graphql',
  endpoint: 'http://localhost:4466',
  secret: "this_is_private"
})

export { prisma as default }

// the object `prisma` provides access to 4 properties: query, mutation, subscription, exists


/*
prisma.query.comments(null, `
{
  id
  text
  author {
    id
    name
  }
}
`).then((data) => {
  console.log('prisma.query.comments: ', JSON.stringify(data, undefined, 2));
})
 */


// Chaining mutations ----------------------------------------------

/*
prisma.mutation.createPost({
  data: {
    title: "My post on GraphQL mutations",
    body: "This is my post on GraphQL mutations. I write very cool blog posts!!!",
    published: true,
    author: {
      connect: {
        email: "foo.bar@foobar.com"
      }
    }
  }
},
  `{
    id
    title
    body
    published
    author {
      id
      name
      email
    }
  }`
).then((data) => {
  console.log(JSON.stringify(data, undefined, 2));
  return prisma.query.users(null,
    `{
      id
      name
      posts {
        title
      }
    }`
  )
}).then((data) => {
  console.log(JSON.stringify(data, undefined, 2));
})
 */

/*
prisma.mutation.updatePost(
  {
    where: { id: "ckbekaorz08tm0760tvztvxuf" },
    data: { title: "GraphQL 104" },
  },
  '{ id title }'
).then((data) => prisma.query.posts(null, '{ id title body published }')
).then((data) => console.log(data))
 */

/* 
const createPostForUser = async (authorId, data) => {
  const userExists = await prisma.exists.User({ id: authorId })

  if (!userExists) {
    throw new Error('User not found')
  }

  const post = await prisma.mutation.createPost({
    data: {
      ...data,
      author: {
        connect: {
          id: authorId
        }
      }
    }
  }, '{ author { id name email posts { id title published } } }');

  return post.author
}
 */

/* 
createPostForUser('ckbd2lckx01hb0760rlqok634', {
  title: 'This is a post about async/await and prisma.exists, part 2.',
  body: 'Well, async/await is good, and prisma.exists is also good.',
  published: false
}).then((user) => {
  console.log(JSON.stringify(user, undefined, 2));
}).catch((error) => {
  console.log(error.message)
})
 */

/*  
const updatePostForUser = async (postId, data) => {
  const postExists = await prisma.exists.Post({ id: postId })

  if (!postExists) {
    throw new Error('Post not found')
  }

  const post = await prisma.mutation.updatePost({
    data,
    where: { id: postId }
  }, '{ author { id name email posts { id title body published } } }');


  return post.author
}

 */

/* 
 updatePostForUser('ckbelz5t809vh0760w7ynfnr2', {
  title: 'This is a post about async/await, part 2',
  body: 'Well, async/await is not like callbacks. Well done job!',
  published: true
}).then((user) => {
  console.log(JSON.stringify(user, undefined, 2));
}).catch((error) => {
  console.log(error.message)
})
 */

/* 
prisma.exists.Comment({
  id: "ckbd2p31601k90760mdh1pmje",
  author: {
    id: "ckbd2lckx01hb0760rlqok634"
  }
}).then((exists) => {
  console.log(exists)
})
 */


