const Subscription = {
  count: {
    subscribe(parent, args, { pubsub }, info) {
      let count = 0

      // every second increase "count"
      // and send to all subscribers
      // on channel "count"
      setInterval(() => {
        count++
        pubsub.publish('count', {
          count
        })
      }, 1000)

      // create a channel named "count" for pub-sub
      return pubsub.asyncIterator('count')
    }
  },
  comment: {
    subscribe(parent, { postId }, { db, pubsub }, info) {
      const post = db.posts.find((post) => post.id === postId)

      if (!post) {
        throw new Error('Post not found.')
      }

      if (!post.published) {
        throw new Error('Post not published.')
      }

      return pubsub.asyncIterator(`comment ${postId}`)
    }
  },
  post: {
    subscribe(parent, args, { pubsub }, info) {
      return pubsub.asyncIterator(`post`)
    }
  }
}

export { Subscription as default }