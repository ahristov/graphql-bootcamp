const users = [{
  id: '1',
  name: 'Atanas',
  email: 'atanas@test.com'
}, {
  id: '2',
  name: 'Dani',
  email: 'dani@test.com',
  age: 23
}, {
  id: '3',
  name: 'Alex',
  email: 'alex@test.com',
  age: 17
}]

const posts = [{
  id: '11',
  title: 'Cooking vegetarian steak',
  body: 'In this video I\'ll show you how to cook vegetarian steak',
  published: true,
  author: '1'
}, {
  id: '12',
  title: 'Cooking chocolate cookies',
  body: 'In this video I\'ll show you how to cook chocolate cookies',
  published: false,
  author: '1'
}, {
  id: '13',
  title: 'Cooking pasta',
  body: 'In this video I\'ll show you how to cook italian stuff!',
  published: true,
  author: '2'
}]

const comments = [{
  id: '101',
  text: 'nice try',
  author: '3',
  post: '11'
}, {
  id: '102',
  text: 'pretty good',
  author: '3',
  post: '12'
}, {
  id: '103',
  text: 'cool stuff',
  author: '3',
  post: '13'
}, {
  id: '104',
  text: 'I want to lean this ...',
  author: '2',
  post: '11'
}]

const db = {
  users,
  posts,
  comments
}

export { db as default }