import { GraphQLServer, PubSub } from 'graphql-yoga'
import db from './db'
import resolvers from './resolvers'

const pubsub = new PubSub()

// Declare the GraphQL server
const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context: {
    db,
    pubsub
  }
})

// Start on default port 4000
server.start(() => {
  console.log('The server is started')
})
