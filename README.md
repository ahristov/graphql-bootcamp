# The Complete Developers Guide to MongoDB

This project contains notes and code from studying [The Modern GraphQL Bootcamp](https://www.udemy.com/course/graphql-bootcamp).

About the course: Learn how to build GraphQL applications using Node.js. Includes Prisma v1, authentication, Apollo Client, and more!

## 🏠 [Homepage](https://bitbucket.org/ahristov/graphql-bootcamp/src)

## Project "graphql-basics"

Run `graphql-basics` using the following commands:

```bash
cd graphql-basics
yarn
yarn dev
```

## Project "graphql-prisma"

To access the _heroku_, use the following links:

[Heroku dev server dashboard](https://dashboard.heroku.com/apps/ahristov-prisma-dev-server)  
[PostgreSQL dashboard](https://data.heroku.com/datastores/313f8cb5-a641-4cb8-9d8f-876e2db07477#administration)  

Run _pgAdmin_, connect to the database, using the connection string from the above URL. You can also find the DB name in it.

Make sure the _prisma server_ is deployed and running:

```bash
cd graphql-prisma/prisma
prisma deploy
cd ../../
```

Run the _nodejs server_:

```bash
cd graphql-prisma
yarn
yarn get-schema
yarn dev
```

## Notes

### Section 1: Course Overview

[PDF Guide](./doc/original.pdf)

GraphQL exposes a single endpoint:

```
POST /graphql (with a GraphQL query)
-->
<-- (data)
```

GraphQL is:

- fast
- flexible (request exactly the data the client needs)
- easy to maintain

Recommended Visual Studio Code Extensions:

- Babel ES6/Es7 : dzannotti.vscode-babel-coloring
- Beautify : hookyqr.beautify
- Docker: ms-azuretools.vscode-docker
- Duplicate action : mrmlnc.vscode-duplicate
- GraphQL fo VSCode: kumar-harsh.graphql-for-vscode
- npm : eg2.vscode-npm-script
- npm Intellisense : christian-kohler.npm-intellisense
- Sublime Text Keymap and Settings Importer : ms-vscode.sublime-keybindings
- Word Count : rido3.wordcount

### Section 2: GraphQL Basics: Schema and Queries

Graphs:

![Graphs](./doc/2020-06-06_11h36_29.png)

### Section 2: Operation arguments

They are 4 arguments that get passed into an resolver functions:

```js

// Type definitions (schema)
const typeDefs = `
  type Query {
    greeting(name: String): String!
  }
`
const resolvers = {
  Query: {
    greeting(parent, args, ctx, info) {
      return 'Hello!'
    },
```

The 4 arguments to all resolver functions are:

- parent: The previous object in the query (the post the user is an author)
- args: Operation arguments supplied
- ctx: Contextual data (logged in user, etc.)
- info: Server operational info

Query with argument:

```js
{
  greeting(name: "Atanas")
}
```

Query without argument:

```js
{
  greeting
}
```

### Section 2: Arrays

Specify operation that returns array of integers. The result cannot be null. The elements cannot be null.

Some static data:

```js
const users = [{
  id: 1,
  name: 'Atanas',
  email: 'atanas@test.com'
}, {
  id: 2,
  name: 'Dani',
  email: 'dani@test.com',
  age: 23
}, {
  id: '3',
  name: 'Alex',
  email: 'alex@test.com',
  age: 17
}]
```

Type definition:

```js
const typeDefs = `
  type Query {
    users(query: String): [User!]!
  }

  type User {
    id: ID!
    name: String!
    email: String!
    age: Int
  }
````

Resolver function:

```js
const resolvers = {
  Query: {
    users(parent, args, ctx, info) {
      if (!args.query) {
        return users
      }

      return users.filter((user) => {
        return user.name.toLowerCase().includes(args.query.toLowerCase())
      })
    },
```

Query without parameters:

```js
{
  users {
    id ...
}
```

Query with parameters:

```js
{
  users(query: "ata") {
    id ...
}
```

Result:

```json
{
  "data": {
    "users": [
      {
        "id": "1",
        "name": "Atanas",
        "email": "atanas@test.com",
        "age": null
      }, ...
```

### Section 2: Mutations

Type definition:

```js
const typeDefs = `
  type Query { ...
  type Mutation {
    createUser(name: String!, email: String!, age: Int): User!
  }
````

Mutation function:

```js
const resolvers = {
  Query: { ... },
  Mutation: {
    createUser(parent, args, ctx, info) {
      const emailTaken = users.some((user) => user.email.toLowerCase() === args.email.toLowerCase())

      if (emailTaken) {
        throw new Error('Email taken.')
      }

      const user = {
        id: uuidv4(),
        name: args.name,
        email: args.email,
        age: args.age
      }

      users.push(user)

      return user
	}
  },
```

Run mutation:

```js
mutation {
  createUser(name: "Rosie", email: "rosie@test.com") {
    id
    name
    email
  }
}

{
  "data": {
    "createUser": {
      "id": "3e636066-f2fc-4cd2-a7bc-f91b592c3479",
      "name": "Rosie",
      "email": "rosie@test.com"
    }
  }
}

{
  "data": null,
  "errors": [
    {
      "message": "Email taken.",
      "locations": [
        {
          "line": 2,
          "column": 3
        }
      ],
      "path": [
        "createUser"
      ]
    }
  ]
}

```

### Section 4: Subscriptions

Needs pub-sub the library:

Needs the `PubSub` imported from `graphql-yoga`.

Add instance of `PubSub` to the _context_:

```js
const pubsub = new PubSub()

// Declare the GraphQL server
const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context: {
    db,
    pubsub
  }
})

```

Create resolver for "Subscription". We define a _channel_ using `asyncIterator()`:

```js
const Subscription = {
  count: {
    subscribe(parent, args, { pubsub }, info) {
      let count = 0

      // every second increase "count"
      // and send to all subscribers
      // on channel "count"
      setInterval(() => {
        count++
        pubsub.publish('count', {
          count
        })
      }, 1000)

      // create a channel named "count" for pub-sub
      return pubsub.asyncIterator('count')
    }
  }
}

export { Subscription as default }
```

Then execute:

```js
subscription {
  count
}
```

```json
{
  "data": {
    "count": 1
  }
}
```

```json
{
  "data": {
    "count": 1
  }
}
```

...

### Section 4: Enums

```js
enum MutationType {
  CREATED
  UPDATED
  DELETED
}

type PostSubscriptionPayload {
  mutation: MutationType!
  data: Post!
}

```

### Section 5: Prisma 101

To create prisma project run:

```
yarn global add prisma
mkdir graphql-prisma
cd graphql-prisma
prisma init prisma
```

The last command did notwork for me. I created manually the files.

See [Prisma, GraphQL, and How to Start](https://medium.com/better-programming/prisma-graphql-how-to-9a3d09419e93).

Inside the _prisma_ directory I created the file _docker-compose.yml_ with the following content:

```yaml
version: '3'
services:
  prisma:
    image: prismagraphql/prisma:1.34
    restart: always
    ports:
      - '4466:4466'
    environment:
      PRISMA_CONFIG: |
        port: 4466
        databases:
          default:
            connector: postgres
            host: ec2-34-193-117-204.compute-1.amazonaws.com
            port: '5432'
            database: d23av6mm9l9mam
            ssl: true
            user: yemtujtcdvarqq
            password: 8731df4427a9e57f8a2d087788234cc4a0e4b21c833fe301876db96d722bd3f1
            migration: true
```

This is to run prisma image on docker. Run the docker image:

```sh
docker-compose up -d
```

Then I initialize prisma:

```sh
prisma init --endpoint http://localhost:4466
```

Rename file _datamodel.prisma_ to _datamodel.graphql_.

Edit file _prisma.yml_, change the "datamodel" to "datamodel.graphql":

```yaml
endpoint: http://localhost:4466
datamodel: datamodel.graphql
```

Run:

```sh
$ prisma deploy


Deploying service `default` to stage `default` to server `local` 4.9s

Changes:

  User (Type)
  + Created type `User`
  + Created field `id` of type `ID!`
  + Created field `name` of type `String!`

Applying changes 16.7s

Your Prisma endpoint is live:

  HTTP:  http://localhost:4466
  WS:    ws://localhost:4466

You can view & edit your data here:

  Prisma Admin: http://localhost:4466/_admin

```

Make changes, run `prisma deploy`.

### Section 5: Integrating Prisma into Node.JS

Initially we have only one subdirectory  in _graphql-prisma_ directory, and this is subdirectory _prisma_.

We start by copying all the files and subdirectories from directory _graphql-basics_, so that we end up with the following:

```bash
~/graphql-bootcamp/graphql-prisma (master)

$ ls -lam
.babelrc, node_modules, package.json, prisma, src, yarn.lock
```

Install package `prisma-bindings`:

```bash
yarn add prisma-binding
```

It provides bindings for nodejs, set of NodeJS methods to interact with GraphQL -
[prisma-binding](https://github.com/prisma-labs/prisma-binding).

Install package `graphql-cli`:

```bash
yarn add graphql-cli
```

It provides a tool for the common GraphQL tasks -
[graphql-cli](https://github.com/Urigo/graphql-cli)

We are using `graphql-cli` to fetch the latest schema from the Prisma service.

In the root of the directory _graphql-prisma_ we create a new file _.graphqlconfig_.
This is a JSON file  that provides 2 pieces of information:

- where does the schema live
- where should it be saved

To make sure VS Code has syntax highlighting, thread the file as JSON etc.
Type Ctrl+Shift+P to open the "Command Palette". You can also click on View->Command Palette... from the menus.
Search for language, pick "JSON".

Create directory _src/generated_, where generated from Prisma files should live.

Edit the file _.graphqlconfig_, add single project "prisma":

```json
{
  "projects": {
    "prisma": {
      "schemaPath": "src/generated/prisma.graphql",
      "extensions": {
        "endpoints": {
          "default": "http://localhost:4466/"
        }
      }
    }
  }
}
```

Two entries we need in this file:

- schemaPath: The file _prisma.graphql_ will have saved the graphql definitions
- an endpoint where the prisma service is running

Some good notes on getting the GraphQL schema from graphql server here:
[get-graphql-whole-schema-query](https://stackoverflow.com/questions/37397886/get-graphql-whole-schema-query).

Edit the tile _package.json_, add new script to get the schema from project "prisma":

```json
  "scripts": {
    "start": "babel-node src/index.js",
    "dev": "nodemon --watch src -e js,graphql --exec yarn start",
    "get-schema": "graphql get-schema -p prisma"
  },

```

Run the script:

```bash
yarn get-schema
```

**NOTE:** I end up using "get-graphql-schema" package, as "graphql get-schema" did not work for me.

Install:

```bash
yarn add get-graphql-schema
```

Edit the tile _package.json_, add new script to get the schema from project "prisma":

```json
  "scripts": {
    "start": "babel-node src/index.js",
    "dev": "nodemon --watch src -e js,graphql --exec yarn start",
    "get-schema": "get-graphql-schema http://localhost:4466 > src/generated/prisma.graphql"
  },

```

Run the script:

```bash
yarn get-schema
```

Add new file _src/prisma.js_. This is for the NodeJS app and repeats the above entries:

```js
import { Prisma } from 'prisma-binding'

const prisma = new Prisma({
  typeDefs: 'src/generated/prisma.graphql',
  endpoint: 'http://localhost:4466',

})

// the object `prisma` provides access to 4 properties: query, mutation, subscription, exists

prisma.query.users(null, `
  {
    id
    name
    email
    posts {
      id
      title
      body
      comments {
        id
        text
      }
    }
  }
`).then((data) => {
  console.log(JSON.stringify(data, undefined, 2));
})```

Import it in _index.js_:

```js
import './prisma'
```

, as we just want it to run. We can now run the app and it will print the list of users from prisma:

```bash

$ yarn dev
...
[
  {
    "id": "ckbd1k83x012g0760hokgcvph",
    "name": "Foo Bar",
    "email": "foo.bar@foobar.com",
    "posts": [
      {
        "id": "ckbd1xg3f01bm0760jfuil1zt",
        "title": "Thiis is a blog post",
        "body": "This is how I write very cool blog posts",
        "comments": []
      },
      {
        "id": "ckbd1y0jj01c60760an12nq67",
        "title": "Thiis is a blog post",
        "body": "This is how I write very cool blog posts",
        "comments": [
          {
            "id": "ckbd2p31601k90760mdh1pmje",
            "text": "This is a comment"
          }
        ]
      }
    ]
  },
  ...

```

### Section 5: async/await example with prisma.exists

Create post for user with check if the user exists:

```js

const createPostForUser = async (authorId, data) => {
  const userExists = await prisma.exists.User({ id: authorId })

  if (!userExists) {
    throw new Error('User not found')
  }

  const post = await prisma.mutation.createPost({
    data: {
      ...data,
      author: {
        connect: {
          id: authorId
        }
      }
    }
  }, '{ author { id name email posts { id title published } } }');

  return post.author
}


createPostForUser('ckbd2lckx01hb0760rlqok634', {
  title: 'This is a post about async/await and prisma.exists, part 2.',
  body: 'Well, async/await is good, and prisma.exists is also good.',
  published: false
}).then((user) => {
  console.log(JSON.stringify(user, undefined, 2));
}).catch((error) => {
  console.log(error.message)
})

```

## Section 5: Customizing type relationships

What happens when a record gets deleted. we have 2 types of behavior:

- SET_NULL (default)
- CASCADE

Because `SET_NULL` is the default, we cannot delete user if the user has posts or comments.

Request:

```js
mutation {
  deleteUser(
    where: { id: "ckbd2lckx01hb0760rlqok634" }
  ) {
    id
    name
    email
  }
}
```

Response:

```json
{
  "data": {
    "deleteUser": null
  },
  "errors": [
    {
      "locations": [
        {
          "line": 2,
          "column": 3
        }
      ],
      "path": [
        "deleteUser"
      ],
      "code": 3042,
      "message": "The change you are trying to make would violate the required relation 'PostToUser' between Post and User",
      "requestId": "local:ckbenmotf0azf0760l3cbxt5o"
    }
  ]
}
```

This is because the corresponding relationships are required:

```js
type Post {
  id: ID! @id
  ...
  author: User!
```

In order to switch to `CASCADE`, we can change the schema:

```js
type User {
  id: ID! @id
  name: String!
  email: String! @unique
  posts: [Post!]! @relation(name: "PostToUser", onDelete: CASCADE)
  comments: [Comment!]! @relation(name: "CommentToUser", onDelete: CASCADE)
}

type Post {
  id: ID! @id
  title: String!
  body: String!
  published: Boolean!
  author: User! @relation(name: "PostToUser", onDelete: SET_NULL)
  comments: [Comment!]!  @relation(name: "CommentToPost", onDelete: CASCADE)
}

type Comment {
  id: ID! @id
  text: String!
  author: User! @relation(name: "CommentToUser", onDelete: SET_NULL)
  post: Post! @relation(name: "CommentToPost", onDelete: SET_NULL)
}
```

Then we run from subdirectory _prisma_:

```bash
prisma deploy
...
Deploying service `default` to stage `default` to server `local` 10.5s
Service is already up to date.
Applying changes 2.7s

Your Prisma endpoint is live:

  HTTP:  http://localhost:4466
  WS:    ws://localhost:4466

You can view & edit your data here:

  Prisma Admin: http://localhost:4466/_admin
```

If we run the mutation in the playground again, we can delete the user.

Request:

```js
mutation {
  deleteUser(
    where: { id: "ckbd2lckx01hb0760rlqok634" }
  ) {
    id
    name
    email
  }
}

```

Response:

```json
{
  "data": {
    "deleteUser": {
      "id": "ckbd2lckx01hb0760rlqok634",
      "name": "Foo Bar",
      "email": "foo.bar11@foobar.com"
    }
  }
}
```

## Section 6: Closing Prisma with secret and token

These steps are to run from "graphql-prisma" directory.

### Protect Prisma

Edit file "prisma/prisma.yml", add secret to the prisma instance:

```yml
endpoint: http://localhost:4466
datamodel: datamodel.graphql
secret: SECRET__COMES__HERE
```

Deploy prisma and generate a token:

```bash
cd prisma
prisma deploy
prisma token
eyJhbGciOiJIUzI1NiIsInR5cCI6Ikp...
cd ..
```

The _secret_ is to use on the NodeJS side.

The _token_ is to use on the GraphQL Playground when connecting directly to Prisma playground (http://localhost:4466/).

### Access protected Prisma Playground via browser using Authorization token

Open the Prisma playground (http://localhost:4466/) and add the token to the headers. Look at the left side at the bottom, click on "HTTP HEADERS". It is a JSON object, edit and add the `Authorization` header:

```js
{
  "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6Ikp..."
}
```

This allows to access to Prisma directly using token as protection.

### Access protected Prisma from the NodeJS server using the secret

Edit file "src/prisma.js", add the _secret_:

```js
import { Prisma } from 'prisma-binding'

const prisma = new Prisma({
  typeDefs: 'src/generated/prisma.graphql',
  endpoint: 'http://localhost:4466',
  secret: "SECRET__COMES__HERE"
})

export { prisma as default }
```

Start the nodejs server:

```bash
yarn dev
```

## Links

[Author's GraphQL demo page](https://graphql-demo.mead.io/)  
[Homepage](https://bitbucket.org/ahristov/graphql-bootcamp/src)  
[The Modern GraphQL Bootcamp](https://www.udemy.com/course/graphql-bootcamp)  
[Prisma](https://www.prisma.io/)  
[BabelJS tryout](https://babeljs.io/repl)  
[GraphQL Spec](http://spec.graphql.org/)  
[GraphQL Latest Spec](http://spec.graphql.org/June2018/)  
[GraphQL Yoga Server](https://github.com/prisma-labs/graphql-yoga)  
[Apollo GraphQL Subscriptions](https://github.com/apollographql/graphql-subscriptions)  
[Start from scratch Prisma, Typescript, PostgeSQL](https://www.prisma.io/docs/getting-started/setup-prisma/start-from-scratch-prisma-migrate-typescript-postgres)  
[Prisma, GraphQL, and How to Start](https://medium.com/better-programming/prisma-graphql-how-to-9a3d09419e93)  
[prisma-binding](https://github.com/prisma-labs/prisma-binding)  
[graphql-cli](https://github.com/Urigo/graphql-cli)  
[graphql-config code](https://github.com/kamilkisiela/graphql-config)  
[graphql-config site](https://graphql-config.com/introduction)  
[get-graphql-whole-schema-query](https://stackoverflow.com/questions/37397886/get-graphql-whole-schema-query)  
[get-graphql-schema](https://github.com/prisma-labs/get-graphql-schema)
